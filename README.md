# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary:  
The program converts a regular Base10 number into a Base62 string, and vice versa.
* Version:  
3.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up:   
Ensure that you have an IDE installed (I'm using NetBeans 8.2)  
Ensure that you have SourceTree installed.  
Preferably, use Firefox.  
  
Run your IDE, run SourceTree, run your WebBrowser (you're probably reading this with it)  
  
Using your IDE, create a New Project of type "Java Application"  
Name it "Base62" and untick the option to generate a main class automatically  
Then on BitBucket, open up my Base62 Branch and select the "Clone" option under Actions on the left.  
Now select the "Clone in SourceTree" option, this will prompt you to Open Link using an application (SourceTree will be suggested)  
Press the Open Link button.  
An additional window will popup in SourceTree, which should detect the Checkout Branch of master of type Git repository.  
There is only one setting to change here, the Destination Path.  
Use the "..." button and navigate to the Java Application project that you created at the beginning of this paragraph. Select the "src" folder.  
So the path should be like this; C:\Users\John M\Documents\NetBeansProjects\Base62\src  
Press the Clone button, this should now proceed with Cloning the files into the destination.  
Once complete, SourceTree should show the Log/History of the repository. Note in your Project src folder, there is now be a ".git" hidden folder.  
Now at the top of SourceTree, press the "Pull" button. Leave all default settings in the window that appears (origin, master) and press OK.  
Check your project src folder again, there should now be a "base62" folder with all of the .java files within.  
Congratulations, you've successfully Cloned the repository!  
  
* Configuration: 
In your IDE, Build the project.  
Now right click on the project name and select Properties.  
In the window that appears, choose the "Run" option on the left.  
On the right, you should see a "Main Class". Press the "Browse" button beside this option.  
There should only be one option to choose from; "base62.Base62". Select it and press OK.  
* Dependencies:   
None.  
* Database configuration:   
None.  
* How to run tests:   
Alter either/both of the variables at the top of the main method,  
long base10Num will be converted into a Base62 String,  
String base62Num will be converted into a Base10 long.  
Build/Compile the code,  
Run the Application, view results in the console.  

A GUI has been provided in Version 3  
Input an AlphaNumeric value into the field provided and press the Button for Next Value or press Enter  
The text at the bottom will update accordingly, with a base10 conversion of the input and the next base62 value  
A warning message displays the maximum value allowed, inputting anything greater than this will crash the software.
* Deployment instructions  

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin:   
John Mitchell,  
midgey17@hotmail.com.  
* Other community or team contact:   
Ultan McPadden,  
umcpadden@yahoo.com.