/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base62;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author John Mitchell
 * @email midgey17@hotmail.com
 * 
 * 
 * Change either of the two variables at the beginning; base10Num, base62Num
 * 
 * The method convertToBase62 takes the base10Num long and converts it to a base62 String
 * The method convertToBase10 takes the base62Num String and converts it to a base10 number
 */
public class Base62 extends Application {    
    /**
     * @param args the command line arguments
     */                    
            
    //TEXT + LABELS
    Text warningText = new Text("Max Value is: aZl8N0y58M6");
    Text resultText = new Text("Result"); 
    Label titleLabel = new Label("Enter AlphaNumeric Value");       
        
    
    public static void main(String[] args) {                                                
        // declare variables
        long base10Num = Long.MAX_VALUE-1; //change this value
        String base62Num = "ZIK0ZH"; //change this value
        
        Number b10Number = new Number();
        Number b62Number = new Number();
        
        Number nextB10Number = new Number();
        Number nextB62Number = new Number();
                                
        //***** Simple test of the method convertToBase62 *****
        System.out.println("********** Base62 Conversion **********");        
        System.out.println(base10Num+" Converted to Base62 = "+b10Number.convertToBase62(base10Num));
        nextB10Number.setValueWithBase10(base10Num+1);
        System.out.println("Next is: "+nextB10Number.base10Value+" Converted to Base62 = "+nextB10Number.base62Value);
        //System.out.println(base10Num+" Converted to base62 = "+convertToBase62(base10Num));  
                
        //***** Simple test of the method convertToBase10 *****
        System.out.println();
        System.out.println("********** Base10 Conversion **********");
        System.out.println(base62Num+" Converted to Base10 = "+b62Number.convertToBase10(base62Num));
        nextB62Number.setValueWithBase10(b62Number.base10Value+1);  
        System.out.println("Next is: "+nextB62Number.base62Value+" Converted to Base10 = "+nextB62Number.base10Value);
        //System.out.println(base62Num+" Converted to Base10 = "+convertToBase10(base62Num));
        //System.out.println("Grand total is: "+convertToBase10(base62Num));
        
        launch(args);
    } //main
    
    
    //*************************************************
    //************************************************* 
    //*************************************************
    
    //GUI
    @Override
    public void start(Stage primaryStage) 
    {
        primaryStage.setTitle("Base62 Calculator");        
        
        //PANES
        GridPane rootPane = new GridPane();
        GridPane topPane = new GridPane();
        BorderPane bottomPane = new BorderPane();
        rootPane.setAlignment(Pos.CENTER); 
        rootPane.setVgap(20);        
        topPane.setAlignment(Pos.CENTER);
        topPane.setVgap(5);                
        
        //USER INPUT FIELDS
        TextField inputField = new TextField();        
        
        //BUTTONS
        Button convertButton = new Button("Get Next Value");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.CENTER);
        HBox.setHgrow(convertButton, Priority.ALWAYS);
        convertButton.setMaxWidth(160);
        hbBtn.getChildren().add(convertButton);
        
        //ACTIONS
        convertButton.setOnAction((ActionEvent e) -> {
            executeInput(inputField.getText());
        } //handle
        );
        
        inputField.setOnKeyPressed((KeyEvent keyEvent) -> {
            if (keyEvent.getCode() == KeyCode.ENTER)  {  
                executeInput(inputField.getText());
            } //if
        } //handle
        );
        
        //ADD OBJECTS TO PANES
        topPane.add(warningText, 0, 0);
        topPane.add(titleLabel, 0, 2);
        topPane.add(inputField, 0, 3);
        topPane.add(hbBtn, 0, 4);        
        bottomPane.setCenter(resultText);
        rootPane.add(topPane, 0, 0);
        rootPane.add(bottomPane, 0, 1);
        
        //SCENE
        Scene scene = new Scene(rootPane, 450, 200);
        primaryStage.setScene(scene);
        scene.getStylesheets().add
        (Base62.class.getResource("base62.css").toExternalForm());
        primaryStage.setMinWidth(450);
        primaryStage.setMinHeight(200);
        primaryStage.show();
        //primaryStage.close(); //uncomment this line to not use the GUI
        
        //ASSIGN CSS IDs
        resultText.setId("result-text"); 
        warningText.setId("warning-text"); 
        
    } //start(Stage)        
    
    public void executeInput(String input)
    {
        String part1, part2; //create two variables for the result string
        Number inputNumber = new Number(); //create a Number Object
        inputNumber.setValueWithBase62(input); //set the Number values with the user input
        part1 = input+" converted to base10 = "+inputNumber.base10Value+"\n"; //first part of result
        if(inputNumber.base10Value < Long.MAX_VALUE) { //check to make sure that the max value hasn't been reached
            inputNumber.setValueWithBase10(inputNumber.base10Value+1); //increase the value by 1
            part2 = "The next base62 value is "+inputNumber.base62Value; //second part of result
        } //if
        else { //don't attempt to increase value beyond max limit
            part2 = "Maximum value reached";
        } //else
        
        resultText.setText(part1+part2); //update the text at the bottom of the GUI
    } //executeInput
    
} //class
