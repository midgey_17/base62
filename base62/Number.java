/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base62;

/**
 *
 * @author John M
 */
public class Number {
    String base62Value; 
    long base10Value;
    String[] base62Digits = {"0","1","2","3","4","5","6","7","8","9",
                                "a","b","c","d","e","f","g","h","i","j",
                                "k","l","m","n","o","p","q","r","s","t",
                                "u","v","w","x","y","z",
                                "A","B","C","D","E","F","G","H","I","J",
                                "K","L","M","N","O","P","Q","R","S","T",
                                "U","V","W","X","Y","Z"};
    
    //convert a regular base10 number into a base62 string
    public String convertToBase62(long number)
    {                
        base10Value = number;
        String total = "";
        long multiplier;
        int digit;      
        int position = 20;  //assuming the length of the string doesn't exceed this position value
                            //increase it if needed
                            //integer may not facilitate larger values however
        
         //determine the position of the largest digit
         multiplier = (long)(Math.pow(62, position)); //this value depends on the position of a digit; which is 62 to the power of
         //System.out.println(number + ">" + multiplier); 
        while(number < multiplier)
        {
            //System.out.println("number:"+number+" < "+"multiplier:"+multiplier);
            position--; //try next position
            multiplier = (long)(Math.pow(62, position)); //update multiplier
        } //largest digit position determined
        
        //loop through each digit
        while(position >= 0)
        {                       
            multiplier = (long)(Math.pow(62, position)); //update multiplier
            if (number > 0)
            {
                digit = (int)(number/multiplier); //how many times does the multiplier divide into the outstanding number               
                number = number%multiplier; //hold onto the remainder value                 
                total += base62Digits[digit]; //add the base62 digit to the total string  
                /*
                //print the information processed at this stage
                System.out.println("Digit = "+digit);
                System.out.println("Remainder = "+remainder);
                System.out.println("Number = "+number);
                System.out.println("Total = "+total);   
                */
            } //if
            else
            {
                total += base62Digits[(int)number]; //add the base62 digit to the total string                
            } //else
            
            position --;                
        } //while there are digits remaining
        
        this.base62Value = total;
        return total;
    } //convertToBase62     
    
    //*************************************************
    //************************************************* 
    //*************************************************
    
    // converts a String into a base10 number
    public long convertToBase10(String number)
    {
        base62Value = number;
        long total = 0; //this will be the grand total to return
        long figure; //variable temporarily used to hold the base10 value for each character
        String[] digits = number.split(""); //split the string into individual characters
        int position = digits.length-1; //get the amount of digits, for determining the multiplier        
        long multiplier; //this value depends on the position of a digit; which is 62 to the power of

        //loop through all digits, converting each individual one into a base10
        //based on the character itself multiplied by its' position
        for(String digit : digits)
        {
            multiplier = (long)(Math.pow(62, position)); //determine the multiplier for the digit's position  
            figure = convertBase62Character(digit)*multiplier; //determine total base10 value of the digit                                                                      
            total += figure; //add current figure's value to the total                                                  

            //print the information processed so far
            /*
            System.out.println("Multiplier = "+multiplier);
            System.out.println("Digit "+digit+" in position "+position+" = "+figure*multiplier);
            System.out.println("Total is "+total+" after adding digit: "+digit);
            */

            position--; //next character position
        }        
        this.base10Value = total;  
        return total;
    } //convertToBase10
           
    //*************************************************
    
    // converts a single base62 character into a base10 integer
    public int convertBase62Character(String digit)
    {        
        for (int i=0; i<base62Digits.length; i++) //loop through all possible characters
        {
            if(base62Digits[i].equals(digit)) //compare the digit to the array to find its' position
            {
                return i; //return the position of the character, which is the base10 value
            } //if
        } //for

        return 0;
    } //convertCharacter
    
    //*************************************************
    //*************************************************
    //*************************************************
    
    //sets the value variables with a new value
    public void setValueWithBase62(String newValue)
    {
        //the following method call also stores the String as the base62 value
        base10Value = convertToBase10(newValue);
    } //setValueWithBase62
    
    public void setValueWithBase10(long newValue)
    {
        //the following method call also stores the int as the base10 value
        base62Value = convertToBase62(newValue);
    } //setValueWithBase10
    
}
